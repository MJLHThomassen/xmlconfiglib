﻿<!--
  The MIT License (MIT)

  Copyright (c) 2015 Maurice Camp, Maarten Thomassen

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
-->

<!--
    Changes:
    18-01-2014: Maarten Thomassen: Initial licenced file.
    28-01-2015: Maarten Thomasen: Added TreeView of settings history.
-->
    
<Window x:Class="XmlConfigLib.Examples.BasicExample.MainWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:xctk="http://schemas.xceed.com/wpf/xaml/toolkit"
        DataContext="{Binding Mode=OneWay, RelativeSource={RelativeSource Self}}"
        Title="MainWindow" Height="480" Width="640">
    <Grid>
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto"/>
            <RowDefinition/>
        </Grid.RowDefinitions>

        <StackPanel Grid.Row="0" Orientation="Horizontal">
            <Button x:Name="UndoButton" Content="Undo" Click="UndoButton_OnClick" Margin="5" Padding="5"/>
            <Button x:Name="RedoButton" Content="Redo" Click="RedoButton_OnClick" Margin="5" Padding="5"/>
            <Button x:Name="ClearButton" Content="Clear" Click="ClearButton_OnClickButton_OnClick" Margin="5" Padding="5"/>
        </StackPanel>

        <TabControl Grid.Row="1">
            <TabItem Header="Properties">
                <Grid Margin="10">
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="*"/>
                    <ColumnDefinition Width="2*"/>
                </Grid.ColumnDefinitions>

                <Grid.RowDefinitions>
                    <RowDefinition Height="Auto"/>
                    <RowDefinition Height="Auto"/>
                    <RowDefinition Height="Auto"/>
                    <RowDefinition Height="Auto"/>
                    <RowDefinition Height="Auto"/>
                    <RowDefinition Height="Auto"/>
                    <RowDefinition Height="Auto"/>
                </Grid.RowDefinitions>


                <Label Grid.Column="0" Grid.Row="0" Content="Bool Property" HorizontalAlignment="Right"/>
                <CheckBox Grid.Column="1" Grid.Row="0" IsChecked="{Binding Settings.BoolProperty, Mode=TwoWay}" HorizontalAlignment="Left" VerticalAlignment="Center"/>

                <Label Grid.Column="0" Grid.Row="1" Content="Int Property"  HorizontalAlignment="Right"/>
                <xctk:IntegerUpDown Grid.Column="1"  Grid.Row="1" Value="{Binding Settings.IntProperty, Mode=TwoWay}"
                            VerticalContentAlignment="Center" />

                <Label Grid.Column="0" Grid.Row="2" Content="Double Property"  HorizontalAlignment="Right"/>
                <xctk:DoubleUpDown Grid.Column="1"  Grid.Row="2" Value="{Binding Settings.DoubleProperty, Mode=TwoWay}"
                           VerticalContentAlignment="Center"/>

                <Label Grid.Column="0" Grid.Row="3" Content="String Property"  HorizontalAlignment="Right"/>
                <TextBox Grid.Column="1"  Grid.Row="3" Text="{Binding Settings.StringProperty, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}"
                 VerticalContentAlignment="Center"/>

                <GroupBox Grid.Column="0" Grid.ColumnSpan="2" Grid.Row="4" Header="NotifyPropertyChangedType">

                    <Grid>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="*"/>
                            <ColumnDefinition Width="2*"/>
                        </Grid.ColumnDefinitions>

                        <Grid.RowDefinitions>
                            <RowDefinition Height="Auto"/>
                            <RowDefinition Height="Auto"/>
                            <RowDefinition Height="Auto"/>
                        </Grid.RowDefinitions>

                        <Label Grid.Column="0" Grid.Row="0" Content="Guid Property" HorizontalAlignment="Right"/>
                        <StackPanel Orientation="Horizontal" Grid.Column="1"  Grid.Row="0">
                            <TextBlock Text="{Binding Settings.NotifyPropertyChangedType.GuidProperty, Mode=TwoWay}" VerticalAlignment="Center"/>
                            <Button Content="Change" Click="ChangeGuidClick" Margin="5"/>
                        </StackPanel>

                        <Label Grid.Column="0" Grid.Row="1" Content="String Property"  HorizontalAlignment="Right"/>
                        <TextBox Grid.Column="1"  Grid.Row="1" Text="{Binding Settings.NotifyPropertyChangedType.StringProperty, Mode=TwoWay,  UpdateSourceTrigger=PropertyChanged}"
                         VerticalContentAlignment="Center"/>

                        <GroupBox Grid.Column="0" Grid.ColumnSpan="2" Grid.Row="2" Header="NestedNotifyPropertyChangedType">

                            <Grid>
                                <Grid.ColumnDefinitions>
                                    <ColumnDefinition Width="*"/>
                                    <ColumnDefinition Width="2*"/>
                                </Grid.ColumnDefinitions>

                                <Grid.RowDefinitions>
                                    <RowDefinition Height="Auto"/>
                                    <RowDefinition Height="Auto"/>
                                </Grid.RowDefinitions>

                                <Label Grid.Column="0" Grid.Row="0" Content="String Property"  HorizontalAlignment="Right"/>
                                <TextBox Grid.Column="1"  Grid.Row="0" Text="{Binding Settings.NotifyPropertyChangedType.NestedNotifyPropertyChangedType.StringProperty, Mode=TwoWay,  UpdateSourceTrigger=PropertyChanged}"
                         VerticalContentAlignment="Center"/>

                                <Button Grid.ColumnSpan="2" Grid.Row="1" Content="new NestedNotifyPropertyChangedType()" Click="NewNestedPropertyChangedType"/>

                            </Grid>
                        </GroupBox>

                    </Grid>
                </GroupBox>

                <GroupBox Grid.Column="0" Grid.ColumnSpan="2" Grid.Row="5" Header="NotNotifyPropertyChangedType">

                    <Grid>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="*"/>
                            <ColumnDefinition Width="2*"/>
                        </Grid.ColumnDefinitions>

                        <Grid.RowDefinitions>
                            <RowDefinition Height="Auto"/>
                            <RowDefinition Height="Auto"/>
                        </Grid.RowDefinitions>

                        <Label Grid.Column="0" Grid.Row="0" Content="Guid Property" HorizontalAlignment="Right"/>
                        <StackPanel Orientation="Horizontal" Grid.Column="1"  Grid.Row="0">
                            <TextBlock Text="{Binding Settings.NotNotifyPropertyChangedType.GuidProperty, Mode=TwoWay}" VerticalAlignment="Center"/>
                            <Button Content="Change" Click="ChangeNotGuidClick" Margin="5"/>
                        </StackPanel>

                        <Label Grid.Column="0" Grid.Row="1" Content="String Property"  HorizontalAlignment="Right"/>
                        <TextBox Grid.Column="1"  Grid.Row="1" Text="{Binding Settings.NotNotifyPropertyChangedType.StringProperty, Mode=TwoWay,  UpdateSourceTrigger=PropertyChanged}"
                         VerticalContentAlignment="Center"/>

                    </Grid>
                </GroupBox>
            </Grid>
        </TabItem>
            <TabItem Header="History">
            <Grid>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition/>
                    <ColumnDefinition/>
                </Grid.ColumnDefinitions>
                <Grid.RowDefinitions>
                    <RowDefinition Height="Auto"/>
                    <RowDefinition/>
                </Grid.RowDefinitions>

                <Label Grid.Column="0" Grid.Row="0">History</Label>
                
                <ScrollViewer Grid.Column="0" Grid.Row="1">
                        <ItemsControl ItemsSource="{Binding History}">
                        <ItemsControl.ItemTemplate>
                            <DataTemplate>
                                <StackPanel Orientation="Horizontal">
                                        <TextBlock Text="{Binding}"/>
                                    </StackPanel>
                            </DataTemplate>
                        </ItemsControl.ItemTemplate>
                    </ItemsControl>
                </ScrollViewer>

                    <Label Grid.Column="1" Grid.Row="0">Future</Label>
                
                    <ScrollViewer Grid.Column="1" Grid.Row="1">
                    <ItemsControl ItemsSource="{Binding Future}">
                        <ItemsControl.ItemTemplate>
                            <DataTemplate>
                                <StackPanel Orientation="Horizontal">
                                        <TextBlock Text="{Binding}" />
                                    </StackPanel>
                            </DataTemplate>
                        </ItemsControl.ItemTemplate>
                    </ItemsControl>
                </ScrollViewer>
            </Grid>
        </TabItem>
        </TabControl>
        
    </Grid>
</Window>
