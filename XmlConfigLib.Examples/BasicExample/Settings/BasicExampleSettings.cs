﻿/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Maurice Camp, Maarten Thomassen
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */

/*
 * Changes:
 * 18-01-2014: Maarten Thomassen: Initial licenced file.
 */

using System;
using XmlConfigLib.Core;

namespace XmlConfigLib.Examples.BasicExample.Settings
{
    public class BasicExampleSettings : XmlFileBase
    {
        private bool _boolProperty;
        private int _intProperty;
        private double _doubleProperty;
        private string _stringProperty;

        private NotifyPropertyChangedType _notifyPropertyChangedType = new NotifyPropertyChangedType
        {
            GuidProperty = Guid.NewGuid(),
            StringProperty = "Hello, NotifyPropertyChangedType!"
        };

        private NotNotifyPropertyChangedType _notNotifyPropertyChangedType = new NotNotifyPropertyChangedType
        {
            GuidProperty = Guid.NewGuid(),

        };

        public bool BoolProperty
        {
            get { return _boolProperty; }
            set
            {
                if (value.Equals(_boolProperty)) return;
                _boolProperty = value;
                OnPropertyChanged();
            }
        }

        public int IntProperty
        {
            get { return _intProperty; }
            set
            {
                if (value == _intProperty) return;
                _intProperty = value;
                OnPropertyChanged();
            }
        }

        public double DoubleProperty
        {
            get { return _doubleProperty; }
            set
            {
                if (value.Equals(_doubleProperty)) return;
                _doubleProperty = value;
                OnPropertyChanged();
            }
        }

        public string StringProperty
        {
            get { return _stringProperty; }
            set
            {
                if (value == _stringProperty) return;
                _stringProperty = value;
                OnPropertyChanged();
            }
        }

        public NotifyPropertyChangedType NotifyPropertyChangedType
        {
            get { return _notifyPropertyChangedType; }
            set
            {
                if (Equals(value, _notifyPropertyChangedType)) return;
                _notifyPropertyChangedType = value;
                OnPropertyChanged();
            }
        }

        public NotNotifyPropertyChangedType NotNotifyPropertyChangedType
        {
            get { return _notNotifyPropertyChangedType; }
            set
            {
                if (Equals(value, _notNotifyPropertyChangedType)) return;
                _notNotifyPropertyChangedType = value;
                OnPropertyChanged();
            }
        }

        public BasicExampleSettings()
        {
        }
    }
}
