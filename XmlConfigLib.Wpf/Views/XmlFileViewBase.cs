﻿/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Maurice Camp, Maarten Thomassen
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */

/*
 * Changes:
 * 17-01-2014: Maurice Camp, Maarten Thomassen: Initial licenced file.
 */

using System;
using System.Windows;
using XmlConfigLib.Core;
using XmlConfigLib.Wpf.ViewModels;

namespace XmlConfigLib.Wpf.Views
{
    public abstract class XmlFileViewBase<TXmlFile> : Window
        where TXmlFile : XmlFileBase, new()
    {

        private readonly string _defaultWindowTitle;

        #region Constructor

        protected XmlFileViewBase(XmlFileViewModelBase<TXmlFile> viewModel)
        {
            DataContext = viewModel;
            _defaultWindowTitle = Title;

            viewModel.OnSettingsChanged += ViewModelOnOnSettingsChanged;
        }

        private void ViewModelOnOnSettingsChanged(object sender, EventArgs eventArgs)
        {
            var viewModel = (XmlFileViewModelBase<TXmlFile>) DataContext;
            OnUiThread(() =>
            {
                Title = _defaultWindowTitle + (String.IsNullOrEmpty(viewModel.CurrentlyLoadedSettingsFile)
                    ? " [Untitled" + (viewModel.SettingsFile.HasChanges ? "*" : "") + "]"
                    : " [" + viewModel.CurrentlyLoadedSettingsFile + (viewModel.SettingsFile.HasChanges ? "*" : "") + "]");
            });
        }

        #endregion

        protected static void OnUiThread(Action action)
        {
            var app = Application.Current;
            if (app == null)
                return;

            var dispatcher = app.Dispatcher;
            if (dispatcher == null)
                return;

            if (dispatcher.CheckAccess())
                action();
            else
                dispatcher.BeginInvoke(action);
        }
    }
}

