﻿/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Maurice Camp, Maarten Thomassen
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */

/*
 * Changes:
 * 17-01-2014: Maurice Camp, Maarten Thomassen: Initial licenced file.
 * 12-09-2015: Maarten Thomassen: Added constructor with parameter, removed virtual keyword from SettingsFile and CurrentlyLoadedSettingsFile
 */

using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Xml;
using XmlConfigLib.Core;
using XmlConfigLib.Core.Annotations;
using XmlConfigLib.Core.Attributes;
using XmlConfigLib.Core.Extensions;
using XmlConfigLib.Wpf.Resources;

namespace XmlConfigLib.Wpf.ViewModels
{
    public abstract class XmlFileViewModelBase<TXmlFile> : INotifyPropertyChanged
        where TXmlFile : XmlFileBase, new()
    {
        private TXmlFile _settingsFile;

        #region Properties

        public TXmlFile SettingsFile
        {
            get { return _settingsFile; }
            set
            {
                if (_settingsFile != null)
                    _settingsFile.PropertyChanged -= SettingsFileOnPropertyChanged;

                _settingsFile = value;

                if (_settingsFile != null)
                    _settingsFile.PropertyChanged += SettingsFileOnPropertyChanged;

                OnPropertyChanged();
            }
        }

        public string CurrentlyLoadedSettingsFile { get; set; }

        #endregion

        #region Events

        public event EventHandler OnSettingsChanged;

        #endregion

        #region Constructor

        protected XmlFileViewModelBase(TXmlFile settingsFile)
        {
            SettingsFile = settingsFile;
        }

        #endregion

        #region Update Functions

        private void UpdateWindowTitle()
        {

        }

        private void SettingsFileOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            UpdateWindowTitle();
            SettingsChanged();
        }

        private void SettingsChanged()
        {
            if (OnSettingsChanged != null)
            {
                OnSettingsChanged(this, new EventArgs());
            }
        }

        #endregion

        #region Saving/Loading

        protected bool SaveConfigWithDialog()
        {
            // Check if the xmlfile has a fileextension attribute
            var type = typeof(TXmlFile);
            var attribute = type.GetAttribute<XmlFileExtensionAttribute>();

            var defaultExt = "";
            var filter = "";

            if (attribute != null)
            {
                defaultExt = attribute.FileExtension;
                filter = attribute.FriendlyFileName + " (" + attribute.FileExtension + ") | " + attribute.FileExtension;
            }

            // Open file dialog
            var fileDialog = new Microsoft.Win32.SaveFileDialog
            {
                DefaultExt = defaultExt,
                Filter = filter
            };

            var dialogResult = fileDialog.ShowDialog() ?? false;
            
            if (!dialogResult)
                return false;

            var filename = fileDialog.FileName;
            var saved = SaveConfig(filename);

            if (saved) return true;

            MessageBox.Show(XmlFileViewResources.Error_Save_File_Message, XmlFileViewResources.Error_Save_File_Title,
                MessageBoxButton.OK, MessageBoxImage.Error);

            return false;
        }

        protected bool SaveConfig(string path)
        {
            try
            {
                XmlHelper.SaveXmlFile(SettingsFile, path);
                CurrentlyLoadedSettingsFile = path;
                UpdateWindowTitle();
            }
            catch (IOException)
            {
                return false;
            }

            return true;
        }

        protected bool LoadConfigWithDialog()
        {
            // Open file dialog
            var type = typeof(TXmlFile);
            var attribute = type.GetAttribute<XmlFileExtensionAttribute>();

            var defaultExt = "";
            var filter = "";

            if (attribute != null)
            {
                defaultExt = attribute.FileExtension;
                filter = attribute.FriendlyFileName + " (" + attribute.FileExtension + ") | " + attribute.FileExtension;
            }

            // Open file dialog
            var fileDialog = new Microsoft.Win32.OpenFileDialog
            {
                DefaultExt = defaultExt,
                Filter = filter
            };

            var dialogResult = fileDialog.ShowDialog() ?? false;

            if (!dialogResult)
                return false;

            var loaded = LoadConfig(fileDialog.FileName);

            if (loaded) return true;

            MessageBox.Show(XmlFileViewResources.Error_Load_File_Message, XmlFileViewResources.Error_Load_File_Title,
                MessageBoxButton.OK, MessageBoxImage.Error);

            return false;
        }

        protected bool LoadConfig(string path)
        {
            try
            {
                SettingsFile = XmlHelper.LoadXmlFile<TXmlFile>(path);
                CurrentlyLoadedSettingsFile = path;
                UpdateWindowTitle();
                return true;
            }
            catch (IOException)
            {
            }
            catch (XmlException)
            {
            }
            catch (InvalidOperationException)
            {
            }
            return false;
        }

        #endregion

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

