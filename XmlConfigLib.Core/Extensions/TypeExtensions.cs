﻿/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Maurice Camp, Maarten Thomassen
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */

/*
 * Changes:
 * 17-01-2015: Maurice Camp, Maarten Thomassen: Initial licenced file.
 * 24-01-2015: Maarten Thomassen: Added HasAttribute method.
 */

using System;
using System.ComponentModel;
using System.Linq;

namespace XmlConfigLib.Core.Extensions
{
    public static class TypeExtensions
    {
        public static TAttribute GetAttribute<TAttribute>(this Type type) 
            where TAttribute : Attribute
        {
            var attrs = TypeDescriptor.GetAttributes(type);
            return attrs.OfType<TAttribute>().FirstOrDefault();
        }

        public static bool HasAttribute<TAttribute>(this Type type)
            where TAttribute : Attribute
        {
            var attrs = TypeDescriptor.GetAttributes(type);
            return attrs.OfType<TAttribute>().Any();
        }

        /// <summary>
        /// Checks if a type implements a specific interface.
        /// </summary>
        /// <param name="type">The type for which to check the interface.</param>
        /// <param name="TInterface">The interface to check for.</param>
        /// <returns>True if the type implements TInterface. False otherwise (or if TInterface is not an interface).</returns>
        public static bool Implements(this Type type, Type TInterface)
        {
            return TInterface.IsInterface && type.GetInterfaces().Contains(TInterface);
        }

        /// <summary>
        /// Checks if a type implements a specific interface.
        /// </summary>
        /// <typeparam name="TInterface">The type of the interface to check for.</typeparam>
        /// <param name="type">The type for which to check the interface.</param>
        /// <returns>True if the type implements TInterface. False otherwise (or if TInterface is not an interface).</returns>
        public static bool Implements<TInterface>(this Type type)
        {
            return type.Implements(typeof(TInterface));
        }
    }
}
