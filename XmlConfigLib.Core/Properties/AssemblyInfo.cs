﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("XmlConfigLib.Core")]
[assembly: AssemblyDescription("A library simplifying saving configuration data for C# / .NET programs to .xml files by providing base classes for saving/loading .xml files.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("XmlConfigLib.Core")]
[assembly: AssemblyCopyright("Copyright ©  2015 Maurice Camp, Maarten Thomassen")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("62784c4f-3027-4e49-a124-ea6a5f185466")]

// Version information for an assembly consists of the following values:
//
//      Major Version
//      Minor Version 
//      Path Version
//      
// See http://semver.org/spec/v2.0.0.html for more information.
[assembly: AssemblyInformationalVersion("0.4.1")]
