﻿/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Maurice Camp, Maarten Thomassen
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */

/*
 * Changes:
 * 21-02-2015: Maarten Thomassen: Initial licenced file.
 * 23-02-2015: Maarten Thomassen: Added tests for inner classes.
 * 25-02-2015: Maarten Thomassen: Changed Namespace.
 * 27-02-2018: Maarten Thomassen: Unified test property classes.
 */

using NUnit.Framework;
using XmlConfigLib.Core.Tests.TestClasses;

namespace XmlConfigLib.Core.Tests
{
    [TestFixture]
    public class PropertyChangedCheckerTests
    {
        private TestClass _testInstance;

        [SetUp]
        public void Initialize()
        {
            _testInstance = new TestClass();
        }

        #region Direct Property Checks
        [Test(Description = "Checks that HasChanges gets set when a property that calls OnPropertyChanged changes.")]
        public void SetHasChangesOnTrackedChange()
        {
            Assert.False(_testInstance.HasChanges);

            _testInstance.Bool = !_testInstance.Bool;

            Assert.True(_testInstance.HasChanges);
        }

        [Test(Description = "Checks that HasChanges gets set when a property that calls OnPropertyChanged with the DontTrackPropertyChanged attribute applied changes.")]
        public void SetHasChangesOnUntrackedChange()
        {
            Assert.False(_testInstance.HasChanges);

            _testInstance.UntrackedProperty = !_testInstance.UntrackedProperty;

            Assert.True(_testInstance.HasChanges);
        }

        [Test(Description = "Checks that HasChanges doesn't get set when a property that calls OnPropertyChanged with the DontCheckPropertyChanged attribute applied changes.")]
        public void DontSetHasChangesOnUncheckedChange()
        {
            Assert.False(_testInstance.HasChanges);

            _testInstance.UncheckedProperty = !_testInstance.UncheckedProperty;

            Assert.False(_testInstance.HasChanges);
        }

        [Test(Description = "Checks that HasChanges doesn't get set when a property that doesn't call OnPropertyChanged changes.")]
        public void DontSetHasChangesOnNotPropertyChangedChange()
        {
            Assert.False(_testInstance.HasChanges);

            _testInstance.NotPropertyChangedProperty =! _testInstance.NotPropertyChangedProperty;

            Assert.False(_testInstance.HasChanges);
        }
        #endregion

        #region Inner INotifyPropertyChanged Property Checks
        [Test(Description = "Checks that HasChanges gets set when an inner property (a property of a type implementing " +
                            "INotifyPropertyChanged which is a property of the PropertyChangedChecker) that calls OnPropertyChanged changes.")]
        public void SetHasChangesOnTrackedInnerChange()
        {
            Assert.False(_testInstance.HasChanges);

            _testInstance.InnerTestClass.Bool = !_testInstance.Bool;

            Assert.True(_testInstance.HasChanges);
        }

        [Test(Description = "Checks that HasChanges gets set when an inner property that calls OnPropertyChanged " +
                            "with the DontTrackPropertyChanged attribute applied changes.")]
        public void SetHasChangesOnUntrackedInnerChange()
        {
            Assert.False(_testInstance.HasChanges);

            _testInstance.InnerTestClass.UntrackedProperty = !_testInstance.UntrackedProperty;

            Assert.True(_testInstance.HasChanges);
        }

        [Test(Description = "Checks that HasChanges doesn't get set when an inner property that calls OnPropertyChanged " +
                            "with the DontCheckPropertyChanged attribute applied changes.")]
        public void DontSetHasChangesOnUncheckedInnerChange()
        {
            Assert.False(_testInstance.HasChanges);

            _testInstance.InnerTestClass.UncheckedProperty = !_testInstance.UncheckedProperty;

            Assert.False(_testInstance.HasChanges);
        }

        [Test(Description = "Checks that HasChanges doesn't get set when an inner property that doesn't call OnPropertyChanged changes.")]
        public void DontSetHasChangesOnNotPropertyChangedInnerChange()
        {
            Assert.False(_testInstance.HasChanges);

            _testInstance.InnerTestClass.NotPropertyChangedProperty = !_testInstance.NotPropertyChangedProperty;

            Assert.False(_testInstance.HasChanges);
        }
        #endregion
    }
}
