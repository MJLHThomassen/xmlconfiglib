﻿/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Maurice Camp, Maarten Thomassen
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */

/*
 * Changes:
 * 21-02-2015: Maarten Thomassen: Initial licenced file.
 * 25-02-2015: Maarten Thomassen: Changed Namespace.
 * 27-02-2018: Maarten Thomassen: Unified test property classes.
 */

using NUnit.Framework;
using XmlConfigLib.Core.Tests.TestClasses;

namespace XmlConfigLib.Core.Tests
{
    [TestFixture]
    public class PropertyChangedTrackerTests
    {
        private TestClass _testInstance;

        [SetUp]
        public void Initialize()
        {
            _testInstance = new TestClass();
        }

        #region Direct Checks
        [Test(Description = "Checks that calling Undo sets a property back to its previous value.")]
        public void UndoSetsTrackedPropertyToPreviousValue()
        {
            var previousValue = _testInstance.Bool;

            _testInstance.Bool = !_testInstance.Bool;
            _testInstance.Undo();

            Assert.AreEqual(previousValue, _testInstance.Bool);
        }

        [Test(Description = "Checks that calling Redo sets a property back to its next value.")]
        public void RedoSetsTrackedPropertyToNextValue()
        {
            _testInstance.Bool = !_testInstance.Bool;

            var nextValue = _testInstance.Bool;

            _testInstance.Undo();
            _testInstance.Redo();

            Assert.AreEqual(nextValue, _testInstance.Bool);
        }

        [Test(Description = "Checks that calling Clear clears property history by checking property values after Undo.")]
        public void ClearPreventsUndo()
        {
            const int referenceValue = 42;
            const int otherValue = 1337;

            // Initialize the value to the reference value.
            _testInstance.Integer = referenceValue;

            // Set it to another value.
            _testInstance.Integer = otherValue;

            // Set it back to the reference value.
            _testInstance.Integer = referenceValue;

            // Clear the history.
            _testInstance.Clear();

            // Check if the property still has the reference value.
            Assert.AreEqual(referenceValue, _testInstance.Integer);

            // Undo the last change, which should change the property back to the other value if clear doesn't work as expected.
            _testInstance.Undo();

            // If clear works as expected, the property still has the reference value.
            Assert.AreEqual(referenceValue, _testInstance.Integer);
        }

        [Test(Description = "Checks that calling Clear clears property future by checking property values after Redo.")]
        public void ClearPreventsRedo()
        {
            const int referenceValue = 42;
            const int otherValue = 1337;

            // Initialize the value to the other value.
            _testInstance.Integer = referenceValue;

            // Set it to the reference value.
            _testInstance.Integer = otherValue;

            // Undo the last change.
            _testInstance.Undo();

            // Clear the history.
            _testInstance.Clear();

            // Check if the property still has the reference value.
            Assert.AreEqual(referenceValue, _testInstance.Integer);

            // Redo the last change, which should set the property back to the other value if clear doesn't work as expected.
            _testInstance.Redo();

            // If clear works as expected, the property still has the reference value.
            Assert.AreEqual(referenceValue, _testInstance.Integer);
        }
        #endregion

        #region Inner INotifyPropertyChanged Checks

        [Test(Description = "Checks that properties of a property class implementing INotifyPropertyChanged which itself is a " +
                            "child of a PropertyChangedTracker are also tracked.")]
        public void InnerINotifyPropertyChangedPropertiesAreTracked()
        {
            var referenceValue = _testInstance.InnerTestClass.Bool;

            _testInstance.InnerTestClass.Bool = !_testInstance.InnerTestClass.Bool;

            var otherValue = _testInstance.InnerTestClass.Bool;

            _testInstance.Undo();

            Assert.AreEqual(referenceValue, _testInstance.InnerTestClass.Bool);

            _testInstance.Redo();

            Assert.AreEqual(otherValue, _testInstance.InnerTestClass.Bool);
        }

        [Test(Description = "Tests for issue #3: https://bitbucket.org/MJLHThomassen/xmlconfiglib/issues/3/when-a-property-implementing." +
                            "See if a tracked INotifyPropertyChanged property, which was replaced but set back to an old reference again doesn't cause an exception")]
        public void SettingInnerINotifyPropertyChangedPropertyBackToOldValueDoesntCrash()
        {
            var referenceValue = _testInstance.InnerTestClass;
            var otherValue = new InnerTestClass();

            _testInstance.InnerTestClass = otherValue;
            _testInstance.InnerTestClass = referenceValue;

            Assert.Pass();
        }

        [Test(Description = "Checks that properties of a property class implementing INotifyPropertyChanged which itself is a " +
                            "child of a PropertyChangedTracker but has the DontTrackPropertyChanged attribute applied are not tracked.")]
        public void UntrackedInnerINotifyPropertyChangedPropertiesAreNotTracked()
        {
            var referenceValue = _testInstance.Bool;
            var untrackedReferenceValue = _testInstance.UntrackedInnerTestClass.Bool;

            _testInstance.Bool = !_testInstance.Bool;
            _testInstance.UntrackedInnerTestClass.Bool = !_testInstance.UntrackedInnerTestClass.Bool;

            var otherValue = _testInstance.Bool;
            var untrackedOtherValue = _testInstance.UntrackedInnerTestClass.Bool;

            _testInstance.Undo();

            Assert.AreEqual(referenceValue, _testInstance.Bool);
            Assert.AreNotEqual(untrackedReferenceValue, _testInstance.UntrackedInnerTestClass.Bool);

            _testInstance.Redo();

            Assert.AreEqual(otherValue, _testInstance.Bool);
            Assert.AreEqual(untrackedOtherValue, _testInstance.UntrackedInnerTestClass.Bool);
        }

        #endregion
    }
}
