﻿/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Maurice Camp, Maarten Thomassen
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */

/*
 * Changes:
 * 27-02-2015: Maarten Thomassen: Initial licenced file.
 */

using XmlConfigLib.Core.Attributes;

namespace XmlConfigLib.Core.Tests.TestClasses
{
    internal class TestClass : PropertyChangedTracker
    {
        private bool _bool;
        private int _integer;
        private bool _uncheckedProperty;
        private bool _untrackedProperty;
        private bool _notPropertyChangedProperty;
        private InnerTestClass _innerTestClass = new InnerTestClass();
        private InnerTestClass _untrackedInnerTestClass = new InnerTestClass();

        public bool Bool
        {
            get { return _bool; }
            set
            {
                if (value.Equals(_bool)) return;
                _bool = value;
                OnPropertyChanged();
            }
        }

        public int Integer
        {
            get { return _integer; }
            set
            {
                if (value == _integer) return;
                _integer = value;
                OnPropertyChanged();
            }
        }

        [DontCheckPropertyChanged]
        public bool UncheckedProperty
        {
            get { return _uncheckedProperty; }
            set
            {
                if (value.Equals(_uncheckedProperty)) return;
                _uncheckedProperty = value;
                OnPropertyChanged();
            }
        }

        [DontTrackPropertyChanged]
        public bool UntrackedProperty
        {
            get { return _untrackedProperty; }
            set
            {
                if (value.Equals(_untrackedProperty)) return;
                _untrackedProperty = value;
                OnPropertyChanged();
            }
        }

        public bool NotPropertyChangedProperty
        {
            get { return _notPropertyChangedProperty; }
            set { _notPropertyChangedProperty = value; }
        }

        public InnerTestClass InnerTestClass
        {
            get { return _innerTestClass; }
            set
            {
                if (Equals(value, _innerTestClass)) return;
                _innerTestClass = value;
                OnPropertyChanged();
            }
        }

        [DontTrackPropertyChanged]
        public InnerTestClass UntrackedInnerTestClass
        {
            get { return _untrackedInnerTestClass; }
            set
            {
                if (Equals(value, _untrackedInnerTestClass)) return;
                _untrackedInnerTestClass = value;
                OnPropertyChanged();
            }
        }
    }
}